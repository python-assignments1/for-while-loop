row=int(input("Enter the number of rows: "))
col=row

for i in range(0,row):
    e,o=2,1
    for j in range(0,col):
        if i%2==0:
            print(e,end=" ")
            e+=2
        else:
            print(o,end=" ")
            o+=2
    print()
